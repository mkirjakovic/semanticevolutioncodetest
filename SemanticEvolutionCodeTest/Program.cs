﻿using System;
using System.IO;
using System.Linq;

namespace SemanticEvolutionCodeTest
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] inputArray = ReadFileData();

            double aritmeticMean = CalculateAritmeticMean(inputArray);

            CalculateStandardDeviation(inputArray, aritmeticMean);

            PrintBuckets(inputArray);

            Console.ReadKey();
        }

        private static void PrintBuckets(double[] inputArray)
        {
            var buckets = new int[] { 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };
            int[] freq = new int[buckets.Length - 1];

            for (int i = 0; i < inputArray.Length; i++)
            {
                for (int j = 0; j < buckets.Length - 1; j++)
                {
                    if (inputArray[i] >= buckets[j] && inputArray[i] < buckets[j + 1])
                    {
                        freq[j]++;
                        break;
                    }
                }
            }

            for (int i = 0; i < freq.Length; i++)
            {
                Console.WriteLine($"Bucket {i * 10} - {i * 10 + 10} contains { freq[i] } elements");
            }
        }

        private static void CalculateStandardDeviation(double[] inputArray, double aritmeticMean)
        {
            double sumOfSquareDifferences = 0;
            for (int i = 0; i < inputArray.Length; i++)
            {
                sumOfSquareDifferences += Math.Pow(inputArray[i] - aritmeticMean, 2);
            }

            double variance = sumOfSquareDifferences / inputArray.Length;
            double stdDeviation = Math.Sqrt(variance);

            Console.WriteLine($"Standard deviation is: {stdDeviation}");
            Console.WriteLine();
        }

        private static double CalculateAritmeticMean(double[] inputArray)
        {
            double sum = 0;
            for (int i = 0; i < inputArray.Length; i++)
            {
                sum += inputArray[i];
            }

            double aritmeticMean = sum / inputArray.Length;

            Console.WriteLine($"Aritmetic mean is: {aritmeticMean}");
            Console.WriteLine();
            return aritmeticMean;
        }

        private static double[] ReadFileData()
        {
            string filename = "SampleData.csv";
            var content = File.ReadAllText(filename);
            var inputArray = content.Split(',', StringSplitOptions.RemoveEmptyEntries)
                      .Select(double.Parse).ToArray();
            return inputArray;
        }
    }
}
